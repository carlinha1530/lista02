/**
 * LISTA2
 * Classe principal da lista. Todas as alteracoes para resolucao devem ser
 * colocadas aqui.
 * 
 * Esta implementacao deverah considerar uma lista estatica, usando vetores como
 * implementacao interna, sendo que os seus elementos devem ser mantidos
 * ordenados pelo campo nome da classe Pessoa (tambem em anexo).
 * 
 * DATA DE ENTREGA: 08/abr/2015 
 * 
 */
public class ListaEstaticaDesordenada {

	Pessoa[] dados; //
	int qtdePessoas = 0;

	/**
	 * 
	 * Definicao do tamanho da lista
	 * 
	 * *** NAO ALTERAR ESTE METODO. ***
	 * 
	 * @param tamanhoLista
	 */
	public ListaEstaticaDesordenada(int tamanhoLista) {
		dados = new Pessoa[tamanhoLista];
	}

	/**
	 * 
	 * Este metodo deve tentar inserir um objeto do tipo Pessoa na proxima
	 * posicao livre da lista, ou seja, sempre no final. Caso tenha inserido
	 * corretamente, o metodo deve retornar true. Caso contrario, deve retornar
	 * false.
	 *  
	 * @param p
	 * @return
	 * 
	 */
	public boolean inserir(Pessoa p) {
		//inserir aki
		if (qtdePessoas != dados.length){
			dados[qtdePessoas] = p;
			++qtdePessoas;
		return true;
		}
		return false;
	}

	/**
	 * 
	 * Este metodo deve retornar o numero de elementos existentes na lista neste
	 * momento.
	 * 
	 * return this.qtdePessoas
	 * @return
	 */
	public int getTamanho() {
		//inserir aki	
		return qtdePessoas;
	}

	/**
	 * 
	 * Este metodo deve ser implementado de tal forma que ele retorne a Pessoa
	 * existente na posicao desejada, sendo que a primeira posicao deve comecar
	 * em zero. Caso seja informada uma posicao que nao esteja definida na
	 * lista ou um numero negativo, deve retornar null.
	 * 
	 * @param posicao
	 * @return
	 */
	public Pessoa getElemento(int posicao) {
		if(posicao >= 0 & posicao < qtdePessoas){
			return dados[posicao];
		}
       return null;
	}

	/**
	 * 
	 * O metodo imprimir deve retornar todos os nomes das pessoas existentes na
	 * lista, na ordem em que foram inseridos na lista. O formato deve seguir o
	 * padrao:
	 * 
	 * {Nome1, Nome2, ..., NomeN}
	 * 
	 * E caso a lista esteja vazia, deve retornar da seguinte forma: {}
	 * @return
	 */
	public String imprimir() {
		String nomes = "";
		if(qtdePessoas != 0){
			for (int i = 0; i< qtdePessoas; i++) {
				if((qtdePessoas)!=(i+1)){
					nomes = nomes + dados[i].nome+", ";
				}else{
					nomes = nomes + dados[i].nome;
					}
				}
			}
	return "{"+nomes+"}";
}

	/**
	 * 
	 * O metodo remover deve retornar o objeto Pessoa existente na posicao
	 * informada como parametro e retirar o objeto da lista. Caso a posicao
	 * desejada nao exista na lista, deve ser retornado null.
	 * @param posicao
	 * @return
	 * ERROR
	 */
	public Pessoa remover(int posicao) {
		if(posicao < qtdePessoas & qtdePessoas > 0){
			//Pessoa t = dados[posicao];
			for (int i = posicao; i < qtdePessoas -1; i++) {
				dados[i] = dados[i+1];
			} 
			//--qtdePessoas;
			//return t;
		}
		return null;
	}

	/**
	 * 
	 * O metodo posicaoNome procura dentre os elementos da lista se existe algum
	 * com o nome informado como parametro e retorna a posicao deste elemento na
	 * lista. Caso nao encontre nenhum elemento com este nome, deve retornar o
	 * valor -1.
	 * 
	 * @param nomeProcurado
	 * @return
	 * 
	 *
	 */
	public int posicaoNome(String nomeProcurado) {
		//inserir aki
		if((dados != null) && (dados.length != 0))
		  for (int i = 0; i < dados.length; i++) {
			if(nomeProcurado == dados[i].nome){
				return i;
			}
		}
		return -1;
	}

	/**
	 * O metodo recuperarIdades deve percorrer todos os elementos da lista e 
	 * retornar as idades e somente as idades, de todas as pessoas que estiverem
	 * na lista. Caso a lista esteja vazia, deve retornar null.
	 * 
	 * @return
	 * ERROR
	 */
	public int[] recuperarIdades() {
		if(qtdePessoas != 0){
		int vetIdade[] = new int[qtdePessoas];
		    for (int i = 0; i < qtdePessoas; i++) {
				vetIdade[i] = dados[i].idade;
				
			}return vetIdade;
		}
	return null; 
  	}

	
	/**
	 * O metodo idadesEmOrdemCrescente deve verificar se todos os elementos da 
	 * lista estao em ordem crescente de idade, e em caso positivo, deve retornar 
	 * true. Se nao estiverem em ordem crescente, deve retornar false.
	 * 
	 * Adote como padrao que uma lista vazia estah sempre ordenada.
	 * 
	 * @return
	 * if (dados == null){
		}else{
			for (int i = dados.length; i > 0 ; i++) {
				if(dados[i].idade == dados[i+1].idade){
					return true;
				}
			}
		}
		return false;
	 * ERROR
	 */
	public boolean idadesEmOrdemCrescente() {
		if (qtdePessoas != 0){
			for (int i = qtdePessoas -1; i > 0 ; i--) {
				if(dados[i].idade < dados[i-1].idade){
					return false;
				}
			}return true;
		}
		return true;
	}
}
